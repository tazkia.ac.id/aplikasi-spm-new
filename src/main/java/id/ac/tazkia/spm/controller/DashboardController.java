package id.ac.tazkia.spm.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class DashboardController {

    @GetMapping("/")
    public String dashboard(){

        return "utama";
    }

    @GetMapping("/dashboard")
    public String dashboard2(){

        return "layout";
    }

}
